rule all:
    input: "a.txt"

# This rule has *no* dependency
rule generate_a_w_log:
    # This is a target
    output: "a.txt"
    # This is the command to generate the respective target
    log: "log_for_a.txt"
    shell:
        """
        time (echo "a" > {output}) > {log}
        """

# Multiple log-files can vbe specified
rule generate_a_w_multiple_log:
    # This is a target
    output: "a_multilog.txt"
    # This is the command to generate the respective target
    log:
        out="log_for_a.out",
        err="log_for_a.err",
    shell:
        """
        time (echo "a" > {output}) 2>{log.err} > {log.out}
        """

