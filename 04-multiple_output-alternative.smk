rule all:
    input: "b_first_out.txt", "b_second_out.txt"

rule generate_some_input:
    output: "{your_pattern}_in.txt"
    shell:
        """
        echo "{wildcards.your_pattern}" > {output}
        """

rule generate_some_output:
    input: "{pattern}_first_in.txt"
    output: "{pattern}_first_out.txt", "{pattern}_second_out.txt"
    message: "GENERATE PATTERNED {output} - Pattern: {wildcards.pattern}"
    shell:
        """
        cat {input} > {output[0]}
        cat {input} {input} > {output[1]}
        """

