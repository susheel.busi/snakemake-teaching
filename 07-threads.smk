NAMES = ["a", "b", "c"]
# at the top of the file
import os
import glob

print("These are the names: ")
print(NAMES)

rule all:
    input: "list_of_generated_output.txt"

rule generate_some_input:
    output: "{your_pattern}_in.txt"
    shell:
        """
        echo "{wildcards.your_pattern}" > {output}
        """

rule generate_some_output:
    input:
        first="{pattern}_first_in.txt",
        second="{pattern}_second_in.txt",
    output: "{pattern}_out.txt"
    message: "GENERATE PATTERNED {output} - Pattern: {wildcards.pattern}"
    shell:
        """
        cat {input.first} {input.second} > {output}
        """

# A default of 4 threads is specified here. This is overriden using the `--cores` CLI option
rule list_generated_output:
    input: expand("{NAME}_out.txt", NAME=NAMES)
    output: "list_of_generated_output.txt"
    threads: 4
    shell:
        """
        echo 'These are all the "out" names' > {output}
        find ./ -name "*_out.txt" -print0 | xargs -0 -P {threads} -I{{}} cat {{}} >> {output}
        """
