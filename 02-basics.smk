rule all:
    input: "b.txt"

rule generate_a:
    output: "a.txt"
    shell:
        """
        echo "a" > {output}
        """

rule generate_b:
    input: "a.txt"
    output: "b.txt"
    message: "GENERATE {output}"
    shell:
        """
        cp {input} {output}
        """

