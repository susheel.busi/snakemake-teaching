rule all:
    input: "b.txt"

rule generate_a_patterned:
    output: "{your_pattern}.txt"
    shell:
        """
        echo "{wildcards.your_pattern}" > {output}
        """

# Will *NOT* work because the `in_pattern` can *not* be inferred
rule generate_b_patterned_wrong:
    input: "{in_pattern}.txt"
    output: "{out_pattern}.txt"
    message: "GENERATE B PATTERNED  WRONG {output} - In pattern: {wildcard.in_pattern}, Out pattern: {wildcards.out_pattern}"
    shell:
        """
        cp {input} {output}
        """

