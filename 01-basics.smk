# This is a dependency-only rule
# By default, the rule at the *top* will serve as the default rule
rule all:
    input: "b.txt"

# This rule has *no* dependency
rule generate_a:
    # This is a target
    output: "a.txt"
    # This is the command to generate the respective target
    shell:
        """
        echo "a" > {output}
        """

# This rule has *one* dependency and *one* target
rule generate_b:
    # This is a dependency
    input: "a.txt"
    # This is another target
    output: "b.txt"
    shell:
        """
        cp {input} {output}
        """
